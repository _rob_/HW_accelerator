#!/usr/bin/python3

import galois_fields as GF

class Transform:
	def __init__(self, m, r, gf):
		self.m = m # 2^m
		self.r = r # prim poly
		self.gf = gf # galois field in which we want to transform


	def find_primitive(self, c):
		# find primitive, that fullfills condition R(c) = 0
		for t in range(1,2**self.m):
			ce = self.gf.exp_elem(c,t)
			if self.r[0] is 0:
				res = 0
			else:
				res = 1
		
			for i in range(1,9):
				if self.r[i] is 1:
					tmp = self.gf.exp_elem(ce, i)
					res = res^tmp
		
			if res is 0:
				break
	
		return t,ce


	def find_transform(self):
		# calculate transform matrix
		a = self.gf.find_root()
		T = [1]
		
		t,b = self.find_primitive(a)
		
		for j in range(1, self.m):
			i = (t*j)%(2**self.m-1)
			# print("i={}".format(i))
			tmp = self.gf.exp_elem(a, i)
			T.append(tmp)
		# reverse T for propper Matrix
		T.reverse()
		# T contains transform vectors in dec. repr.
		return T
	
	
