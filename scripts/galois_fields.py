#!/usr/bin/python3

class GF24:
	
	def mult_b4(self, a, b):
		poly = 19
		p = 0	
		while a and b:
			if (b & 0x01):
				p ^= a
			b >>= 1
			c = a & 0x8
			a <<= 1
			if c != 0:
				a ^= poly	
		return p
	
	
	def exp_ele_b4(self, a, n):
		c = a
		for i in range(0,n-1):
			c = mult_b4(a,c)
		return c
	
	def find_root(self):
		for a in range(2, 16):
			values = []
			c = a
			while c not in values:
				values.append(c)
				c = self.mult_b4(a, c)
			if len(values) is 15:
				break
		return a


class GF28:

	def mult_b8(self, a, b):
		poly = 285
		p = 0
		while a and b:
			if (b & 0x01):
				p ^= a
			b >>= 1
			c = a & 0x80
			a <<= 1
			if c != 0:
				a ^= poly	
		return p


	def exp_ele_b8(self, a, n):
		c = a
		for i in range(0,n-1):
			c = self.mult_b8(a,c)
		return c


	def find_root(self):
		for a in range(2, 256):
			values = []
			c = a
			while c not in values:
				values.append(c)
				c = self.mult_b8(a, c)
			if len(values) is 255:
				break
		return a
		
	
	def create_base(self, root):
		base = [1, root]
		c = root
		for i in range(2,8):
			c = self.mult_b8(c, root)
			base.append(c)
		return base


class GF242:

	def add_b4(self, a, b):
		return a^b

	def mult_b4(self, a, b):
		poly = 19
		p = 0	
		while a and b:
			if (b & 0x01):
				p ^= a
			b >>= 1
			c = a & 0x8
			a <<= 1
			if c != 0:
				a ^= poly	
		return p


	def mult_b42(self, a, b):
		a0 = a&0xf
		a1 = (a>>4)&0xf
		b0 = b&0xf
		b1 = (b>>4)&0xf
		p0 = 9
		
		t1 = self.mult_b4(a0, b0)
		t2 = self.mult_b4(a1, b1)
		c0 = self.add_b4(t1, self.mult_b4(p0, t2))
		
		t1 = self.add_b4(a1, a0)
		t2 = self.add_b4(b1, b0)
		t21 = self.mult_b4(t1,t2)
		t3 = self.mult_b4(a0, b0)
		c1 = self.add_b4(t21, t3)
		
		# print("c0 = {}".format(c0))
		# print("c1 = {}".format(c1))		
		return c0|(c1<<4)
		
	
	def exp_elem(self, a, n):
		c = a
		for i in range(0,n-1):
			c = self.mult_b42(a,c)
		return c
		

	def find_root(self):
		for a in range(1, 20):
			values = []
			c = a
			while c not in values:
				values.append(c)
				c = self.mult_b42(a, c)
			if len(values) is 255:
				return a
		return 0
			
		
