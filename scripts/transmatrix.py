#!/usr/bin/python3

import numpy as np

import galois_fields as GF
import transform as TR


def calc_transform(T):
	_T = np.asarray(T, dtype=np.uint8)
	_Tb = np.unpackbits(_T, axis=1)
	_Tbinv = np.linalg.inv(_Tb)%2 # matrix inversion
	# better readability for writing matrices in TIE code
	return np.transpose(_Tb), np.transpose(_Tbinv.astype(np.dtype("b")))
	
			

def main():
	print("___________________________")
	
	# constants
	r = [1, 0, 1, 1, 1, 0, 0, 0, 1] # polynomial in binary representation
	m = 8 # degree of galois field 2^m
	
	gf242 = GF.GF242()
	trans = TR.Transform(m,r, gf242)
	T = trans.find_transform()
	
	# print(T)
	# prepare array for numpy
	_T = []
	for i in range(0,m):
		_T.append([T[i]])
		
	T, Tinv = calc_transform(_T)
	print ("T:")
	print (T)
	print ("Tinv:")
	print (Tinv)
	
	
if __name__ == "__main__":
	main()
