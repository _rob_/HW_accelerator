// B8_MULTIPLY

// states
state PRIME_b4 4 4'h3 add_read_write
state PRIME_b8 8 8'h1d add_read_write
state PRIME_b16 16 16'h100b add_read_write

state COEFF 256 add_read_write shared_or

state lambda_b4 4 4'b1001 add_read_write
state lambda_b8 8 8'he8 add_read_write

state T8 64 64'b0010000001100100000110101001000000100110100110100010001000001011 add_read_write
state T8i 64 64'b0010010011001010100000000011010010010110000010101000001000010101 add_read_write

//1111101011001000101100101100100010010010010010001000000000000000010100101100100000010110100000001111000011100000000101110111111001111101011001000101100101100100010010010010010001000000000000000010100101100100000010110100000001111000011100001000101110111111 add_read_write
state T16 256 256'b1111101011001000101100101100100010010010010010001000000000000000010100101100100000010110100000001111000011100000000101110111111001111101011001000101100101100100010010010010010001000000000000000010100101100100000010110100000001111000011100001000101110111111 add_read_write
//0001000000000000000000000001000001011000000100000101100001001000110000000001000001011000110100000011110010001000101001001011010000111000000100000101100000101000011100100111000011101010000000100000110011101000000101101110010001000011111100101011000110110001 add_read_write
state T16i 256 256'b0001000000000000000000000001000001011000000100000101100001001000110000000001000001011000110100000011110010001000101001001011010000111000000100000101100000101000011100100111000011101010000000100000110011101000000101101110010001000011111100101011000110110001 add_read_write

// regfiles
regfile reg_simd 128 2 regs


// FLIX Instruction definition
format flix32 32 {slot_a, slot_b, slot_c}
slot_opcodes slot_a {tie_load_reg_b8, tie_load_reg_b16, tie_store_reg_b8, tie_store_reg_b16, tie_load_coeff}
slot_opcodes slot_b {tie_load_reg_b8, tie_load_reg_b16, tie_store_reg_b8, tie_store_reg_b16, tie_load_coeff}
slot_opcodes slot_c {tie_16x_b8_mult}

// load store
operation tie_load_reg_b8 {in AR *addr, in AR offset, out reg_simd regval} {out VAddr, in MemDataIn128} {
	assign VAddr = TIEadd(addr, offset, 1'b0);
	
	wire[7:0] tmp1 = MemDataIn128[7:0];
	wire[7:0] tmp2 = MemDataIn128[15:8];
	wire[7:0] tmp3 = MemDataIn128[23:16];
	wire[7:0] tmp4 = MemDataIn128[31:24];
	wire[7:0] tmp5 = MemDataIn128[39:32];
	wire[7:0] tmp6 = MemDataIn128[47:40];
	wire[7:0] tmp7 = MemDataIn128[55:48];
	wire[7:0] tmp8 = MemDataIn128[63:56];
	wire[7:0] tmp9 = MemDataIn128[71:64];
	wire[7:0] tmp10 = MemDataIn128[79:72];
	wire[7:0] tmp11 = MemDataIn128[87:80];
	wire[7:0] tmp12 = MemDataIn128[95:88];
	wire[7:0] tmp13 = MemDataIn128[103:96];
	wire[7:0] tmp14 = MemDataIn128[111:104];
	wire[7:0] tmp15 = MemDataIn128[119:112];
	wire[7:0] tmp16 = MemDataIn128[127:120];
	
	assign regval = {tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8, tmp9, tmp10, tmp11, tmp12, tmp13, tmp14, tmp15, tmp16};
}

operation tie_load_reg_b16 {in AR *addr, in AR offset, out reg_simd regval} {out VAddr, in MemDataIn128} {
	assign VAddr = TIEadd(addr, {offset[14:0], 1'b0}, 1'b0);
	
	wire[15:0] tmp1 = MemDataIn128[15:0];
	wire[15:0] tmp2 = MemDataIn128[31:16];
	wire[15:0] tmp3 = MemDataIn128[47:32];
	wire[15:0] tmp4 = MemDataIn128[63:48];
	wire[15:0] tmp5 = MemDataIn128[79:64];
	wire[15:0] tmp6 = MemDataIn128[95:80];
	wire[15:0] tmp7 = MemDataIn128[111:96];
	wire[15:0] tmp8 = MemDataIn128[127:112];
	
	assign regval = {tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8};
}

operation tie_store_reg_b8 {in AR *addr, in AR offset, in reg_simd regval} {out VAddr, out MemDataOut128} {
	assign VAddr = TIEadd(addr, offset, 1'b0);
	
	wire[7:0] tmp1 = regval[7:0];
	wire[7:0] tmp2 = regval[15:8];
	wire[7:0] tmp3 = regval[23:16];
	wire[7:0] tmp4 = regval[31:24];
	wire[7:0] tmp5 = regval[39:32];
	wire[7:0] tmp6 = regval[47:40];
	wire[7:0] tmp7 = regval[55:48];
	wire[7:0] tmp8 = regval[63:56];
	wire[7:0] tmp9 = regval[71:64];
	wire[7:0] tmp10 = regval[79:72];
	wire[7:0] tmp11 = regval[87:80];
	wire[7:0] tmp12 = regval[95:88];
	wire[7:0] tmp13 = regval[103:96];
	wire[7:0] tmp14 = regval[111:104];
	wire[7:0] tmp15 = regval[119:112];
	wire[7:0] tmp16 = regval[127:120];
	
	assign MemDataOut128 = {tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8, tmp9, tmp10, tmp11, tmp12, tmp13, tmp14, tmp15, tmp16};
}

operation tie_store_reg_b16 {in AR *addr, in AR offset, in reg_simd regval} {out VAddr, out MemDataOut128} {
	assign VAddr = TIEadd(addr, {offset[14:0], 1'b0}, 1'b0);
	
	wire[15:0] tmp1 = regval[15:0];
	wire[15:0] tmp2 = regval[31:16];
	wire[15:0] tmp3 = regval[47:32];
	wire[15:0] tmp4 = regval[63:48];
	wire[15:0] tmp5 = regval[79:64];
	wire[15:0] tmp6 = regval[95:80];
	wire[15:0] tmp7 = regval[111:96];
	wire[15:0] tmp8 = regval[127:112];
	
	assign MemDataOut128 = {tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8};
}

operation tie_load_coeff {in AR co, in BR shift} {in PRIME_b16, in PRIME_b8, out COEFF} {
	
	wire[15:0] a0 = co;
	wire[15:0] a1 = TIEmux(shift, TIEmux(a0[15], {a0[14:0], 1'b0}, {a0[14:0], 1'b0}^PRIME_b16), TIEmux(a0[7], {a0[6:0], 1'b0}, {a0[6:0], 1'b0}^PRIME_b8));
	wire[15:0] a2 = TIEmux(shift, TIEmux(a1[15], {a1[14:0], 1'b0}, {a1[14:0], 1'b0}^PRIME_b16), TIEmux(a1[7], {a1[6:0], 1'b0}, {a1[6:0], 1'b0}^PRIME_b8));
	wire[15:0] a3 = TIEmux(shift, TIEmux(a2[15], {a2[14:0], 1'b0}, {a2[14:0], 1'b0}^PRIME_b16), TIEmux(a1[7], {a2[6:0], 1'b0}, {a2[6:0], 1'b0}^PRIME_b8));
	wire[15:0] a4 = TIEmux(shift, TIEmux(a3[15], {a3[14:0], 1'b0}, {a3[14:0], 1'b0}^PRIME_b16), TIEmux(a3[7], {a3[6:0], 1'b0}, {a3[6:0], 1'b0}^PRIME_b8));
	wire[15:0] a5 = TIEmux(shift, TIEmux(a4[15], {a4[14:0], 1'b0}, {a4[14:0], 1'b0}^PRIME_b16), TIEmux(a3[7], {a4[6:0], 1'b0}, {a4[6:0], 1'b0}^PRIME_b8));
	wire[15:0] a6 = TIEmux(shift, TIEmux(a5[15], {a5[14:0], 1'b0}, {a5[14:0], 1'b0}^PRIME_b16), TIEmux(a5[7], {a5[6:0], 1'b0}, {a5[6:0], 1'b0}^PRIME_b8));
	wire[15:0] a7 = TIEmux(shift, TIEmux(a6[15], {a6[14:0], 1'b0}, {a6[14:0], 1'b0}^PRIME_b16), TIEmux(a6[7], {a6[6:0], 1'b0}, {a6[6:0], 1'b0}^PRIME_b8));
	wire[15:0] a8 = TIEmux(shift, TIEmux(a7[15], {a7[14:0], 1'b0}, {a7[14:0], 1'b0}^PRIME_b16), 16'b0);
	wire[15:0] a9 = TIEmux(shift, TIEmux(a8[15], {a8[14:0], 1'b0}, {a8[14:0], 1'b0}^PRIME_b16), 16'b0);
	wire[15:0] a10 = TIEmux(shift, TIEmux(a9[15], {a9[14:0], 1'b0}, {a9[14:0], 1'b0}^PRIME_b16), 16'b0);
	wire[15:0] a11 = TIEmux(shift, TIEmux(a10[15], {a10[14:0], 1'b0}, {a10[14:0], 1'b0}^PRIME_b16), 16'b0);
	wire[15:0] a12 = TIEmux(shift, TIEmux(a11[15], {a11[14:0], 1'b0}, {a11[14:0], 1'b0}^PRIME_b16), 16'b0);
	wire[15:0] a13 = TIEmux(shift, TIEmux(a12[15], {a12[14:0], 1'b0}, {a12[14:0], 1'b0}^PRIME_b16), 16'b0);
	wire[15:0] a14 = TIEmux(shift, TIEmux(a13[15], {a13[14:0], 1'b0}, {a13[14:0], 1'b0}^PRIME_b16), 16'b0);
	wire[15:0] a15 = TIEmux(shift, TIEmux(a14[15], {a14[14:0], 1'b0}, {a14[14:0], 1'b0}^PRIME_b16), 16'b0);
	
	assign COEFF = {a15, a14, a13, a12, a11, a10, a9, a8, a7, a6, a5, a4, a3, a2, a1, a0};
}

function[15:0] tie_mult_b16 ([255:0] a, [15:0] b, [0:0] flag) {

	wire[15:0] p0 =  TIEmux(flag[0], TIEmux(b[0], 16'b0, a[15:0]),       TIEmux(b[0],  16'b0,  {a[7:0], a[7:0]}));
	wire[15:0] p1 =  TIEmux(flag[0], TIEmux(b[1],  p0,  p0^a[31:16]),    TIEmux(b[1],  p0,  {p0[15:8], p0[7:0]^a[23:16]}));
	wire[15:0] p2 =  TIEmux(flag[0], TIEmux(b[2],  p1,  p1^a[47:32]),    TIEmux(b[2],  p1,  {p0[15:8], p1[7:0]^a[39:32]}));
	wire[15:0] p3 =  TIEmux(flag[0], TIEmux(b[3],  p2,  p2^a[63:48]),    TIEmux(b[3],  p2,  {p0[15:8], p2[7:0]^a[55:48]}));
	wire[15:0] p4 =  TIEmux(flag[0], TIEmux(b[4],  p3,  p3^a[79:64]),    TIEmux(b[4],  p3,  {p0[15:8], p3[7:0]^a[71:64]}));
	wire[15:0] p5 =  TIEmux(flag[0], TIEmux(b[5],  p4,  p4^a[95:80]),    TIEmux(b[5],  p4,  {p0[15:8], p4[7:0]^a[87:80]}));
	wire[15:0] p6 =  TIEmux(flag[0], TIEmux(b[6],  p5,  p5^a[111:96]),   TIEmux(b[6],  p5,  {p0[15:8], p5[7:0]^a[103:96]}));
	wire[15:0] p7 =  TIEmux(flag[0], TIEmux(b[7],  p6,  p6^a[127:112]),  TIEmux(b[7],  p6,  {p0[15:8], p6[7:0]^a[119:112]}));
	wire[15:0] p8 =  TIEmux(flag[0], TIEmux(b[8],  p7,  p7^a[143:128]),  TIEmux(b[8], {8'b0, p7[7:0]}, {a[7:0], p7[7:0]}));
	wire[15:0] p9 =  TIEmux(flag[0], TIEmux(b[9],  p8,  p8^a[159:144]),  TIEmux(b[9],  p8,  {p8[15:8]^a[23:16],  p8[7:0]}));
	wire[15:0] p10 = TIEmux(flag[0], TIEmux(b[10], p9,  p9^a[175:160]),  TIEmux(b[10], p9,  {p9[15:8]^a[39:32],  p8[7:0]}));
	wire[15:0] p11 = TIEmux(flag[0], TIEmux(b[11], p10, p10^a[191:176]), TIEmux(b[11], p10, {p10[15:8]^a[55:48], p8[7:0]}));
	wire[15:0] p12 = TIEmux(flag[0], TIEmux(b[12], p11, p11^a[207:192]), TIEmux(b[12], p11, {p11[15:8]^a[71:64], p8[7:0]}));
	wire[15:0] p13 = TIEmux(flag[0], TIEmux(b[13], p12, p12^a[223:208]), TIEmux(b[13], p12, {p12[15:8]^a[87:80], p8[7:0]}));
	wire[15:0] p14 = TIEmux(flag[0], TIEmux(b[14], p13, p13^a[239:224]), TIEmux(b[14], p13, {p13[15:8]^a[103:96], p8[7:0]}));
	wire[15:0] p15 = TIEmux(flag[0], TIEmux(b[15], p14, p14^a[255:240]), TIEmux(b[15], p14, {p14[15:8]^a[119:112], p8[7:0]}));
	
	assign tie_mult_b16 = p15;
}


// operations
operation tie_16x_b8_mult {in reg_simd source, inout reg_simd encoded} {in COEFF} {

	wire[15:0] res1 = tie_mult_b16(COEFF, source[15:0],    1'b1)^encoded[15:0];
	wire[15:0] res2 = tie_mult_b16(COEFF, source[31:16],   1'b1)^encoded[31:16];
	wire[15:0] res3 = tie_mult_b16(COEFF, source[47:32],   1'b1)^encoded[47:32];
	wire[15:0] res4 = tie_mult_b16(COEFF, source[63:48],   1'b1)^encoded[63:48];
	wire[15:0] res5 = tie_mult_b16(COEFF, source[79:64],   1'b1)^encoded[79:64];
	wire[15:0] res6 = tie_mult_b16(COEFF, source[95:80],   1'b1)^encoded[95:80];
	wire[15:0] res7 = tie_mult_b16(COEFF, source[111:96],  1'b1)^encoded[111:96];
	wire[15:0] res8 = tie_mult_b16(COEFF, source[127:112], 1'b1)^encoded[127:112];
	
	assign encoded = {res8, res7, res6, res5, res4, res3, res2, res1};
}

operation tie_8x_b16_mult {in reg_simd source, inout reg_simd encoded} {in COEFF} {
	
	wire[15:0] res1 = tie_mult_b16(COEFF, source[15:0],    1'b0) ^ encoded[15:0];
	wire[15:0] res2 = tie_mult_b16(COEFF, source[31:16],   1'b0) ^ encoded[31:16];
	wire[15:0] res3 = tie_mult_b16(COEFF, source[47:32],   1'b0) ^ encoded[47:32];
	wire[15:0] res4 = tie_mult_b16(COEFF, source[63:48],   1'b0) ^ encoded[63:48];
	wire[15:0] res5 = tie_mult_b16(COEFF, source[79:64],   1'b0) ^ encoded[79:64];
	wire[15:0] res6 = tie_mult_b16(COEFF, source[95:80],   1'b0) ^ encoded[95:80];
	wire[15:0] res7 = tie_mult_b16(COEFF, source[111:96],  1'b0) ^ encoded[111:96];
	wire[15:0] res8 = tie_mult_b16(COEFF, source[127:112], 1'b0) ^ encoded[127:112];
	
	assign encoded = {res8, res7, res6, res5, res4, res3, res2, res1};
}
