//#define DBG_FILE main_c
//#include "thlib.h"
//#include "logcollect.h"

#include <stdint.h>
#include <stdio.h>
#include <xtensa/tie/b8_multiply.h>
#include <xtensa/tie/inverse.h>

#define PACKETSIZE	1
#define GENSIZE		1  // 128

//#define TESTROUNDS	1000000
#define TESTROUNDS	1 //10

int b8_test(uint8_t* encoded, uint8_t* source, uint8_t* coefficients)
{
	int i, j, result;
	int round;
	register reg_simd reg_source, reg_encoded;

	for (round = 0; round < TESTROUNDS; round++) {
		for (i = 0; i < GENSIZE; i++) {
			tie_load_coeff(coefficients[i], 1);
			for (j = 0; j < PACKETSIZE; j+=16) {
				reg_encoded = tie_load_reg_b8(encoded, j);
				reg_source = tie_load_reg_b8(source, j);
				tie_16x_b8_mult(reg_source, reg_encoded);
				tie_store_reg_b8(encoded, j, reg_encoded);
			}
		}
	}

	return 0;
}

int b16_test(uint16_t* encoded, uint16_t* source, uint16_t* coefficients)
{
	int i, j, result;
	int round;
	register reg_simd reg_source, reg_encoded;

	for (round = 0; round < TESTROUNDS; round++) {
		for (i = 0; i < GENSIZE; i++) {
			tie_load_coeff(coefficients[i], 0);
			for (j = 0; j < PACKETSIZE; j+=8) {
				reg_encoded = tie_load_reg_b16(encoded, j);
				reg_source = tie_load_reg_b16(source, j);
				tie_8x_b16_mult(reg_source, reg_encoded);
				tie_store_reg_b16(encoded, j, reg_encoded);
			}
		}
	}

	return 0;
}

int main(int argc, char *argv[])
{
	int i,j;

	uint8_t encoded[PACKETSIZE], source[PACKETSIZE], coefficients[GENSIZE];

	for (j = 0; j < PACKETSIZE; j++) {
		source[j] = j | 0xf0;
		encoded[j] = j | 0xf0;
	}

	for (i = 0; i < GENSIZE; i++)
		coefficients[i] = i | 0xf0;


	int res;
    res = b8_test(encoded, source, coefficients);

    for (j = 0; j < PACKETSIZE; j++)
   		printf("encoded[%d] = 0x%x\n", j, encoded[j]);


    uint16_t source16[PACKETSIZE] __attribute__ ((aligned (8)));
    uint16_t encoded16[PACKETSIZE] __attribute__ ((aligned (8)));
    uint16_t coefficients16[GENSIZE] __attribute__ ((aligned (8)));


    for (j = 0; j < PACKETSIZE; j++) {
    	source16[j] = j | 0xff00;
    	encoded16[j] = j | 0xff00;
    }

    for (i = 0; i < GENSIZE; i++)
   	   coefficients16[i] = i | 0xff00;

    int res2;
    res2 = b16_test(encoded16, coefficients16, source16);

	for (j = 0; j < PACKETSIZE; j++)
        printf("encoded16[%d] = 0x%x\n", j, encoded16[j]);



    //uint8_t a8 = 0xfd;
    //uint8_t b8 = tie_inverse_b8(a8);
    //uint16_t a16 = 0x5295;
    //uint16_t b16 = tie_inverse_b16(a16);
    //printf("a8 = 0x%x\n", a8);
    //printf("b8 = 0x%x\n", b8);
    //printf("a16 = 0x%x\n", a16);
    //printf("b16 = 0x%x\n", b16);

    return 0;
}
